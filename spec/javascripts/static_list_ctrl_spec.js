'use strict';

describe('Testing', function(){
  it("should init correctly", function() {
    browser().navigateTo('/');

    expect(element("#static-list li").count()).toBe(3)
  });

  it("should correctly init elements on the list", function(){
    browser().navigateTo('/');
    
    expect(element("#static-list li:eq(0)").text()).toContain("item1")
    expect(element("#static-list li:eq(1)").text()).toContain("item2")
    expect(element("#static-list li:eq(2)").text()).toContain("item3")
  });
});
