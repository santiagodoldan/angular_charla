Demo.controller('DemoCtrl', ['$scope', function($scope){

  $scope.showStaticList = function() {
    $scope.static_list_visible = true;
    $scope.filter_list_visible = false;
  };

  $scope.showFilterList = function() {
    $scope.static_list_visible = false;
    $scope.filter_list_visible = true;
  };

  $scope.showStaticList();

}]);