Demo.factory('Item', ['$http', '$q', function($http, $q) {
  return {
    query: function() {
      var deferred = $q.defer();

      $http.get('/items').success(function(data) {
        deferred.resolve(data);
      });

      return deferred.promise;
    }
  }
}]);