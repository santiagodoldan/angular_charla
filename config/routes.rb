Angular::Application.routes.draw do

  resources :items, only: [:index]

  get 'service_list', to: 'home#service_list', as: 'service_list'

  root to: 'home#index'
end
